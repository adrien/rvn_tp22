# -*- mode: ruby -*-
# vi: set ft=ruby :

# Script de configuration Nomad
# Fait par A. Luxey-Bitri en décembre 2023
# Inspiré des docs Hashcorp
# Licence : Domaine public

# ÉTUDIANTS : Ça peut vous donner quelques idées, mais le script est périmé, inspirez-vous plutôt `install_nomad_consul.sh`

# Provisioning
$script = <<-'SCRIPT'
echo "## Provisioning VM ##"
echo "Installing dependencies..."
sudo apt-get update
sudo apt-get install -y unzip curl jq dnsutils
echo "Determining Consul version to install ..."
CHECKPOINT_URL="https://checkpoint-api.hashicorp.com/v1/check"
if [ -z "$CONSUL_VERSION" ]; then
  CONSUL_VERSION=$(curl -s "${CHECKPOINT_URL}"/consul | jq .current_version | tr -d '"')
fi
if [ -z "$NOMAD_VERSION" ]; then
  NOMAD_VERSION=$(curl -s "${CHECKPOINT_URL}"/nomad | jq .current_version | tr -d '"')
fi
echo "Fetching Consul version ${CONSUL_VERSION} ..."
cd /tmp/
curl -s https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip -o consul.zip
echo "Installing Consul version ${CONSUL_VERSION} ..."
unzip consul.zip
sudo chmod +x consul
sudo mv consul /usr/bin/consul
sudo mkdir /etc/consul.d
sudo chmod a+w /etc/consul.d

echo "Fetching Nomad version ${NOMAD_VERSION} ..."
cd /tmp/
curl -s https://releases.hashicorp.com/nomad/${NOMAD_VERSION}/nomad_${NOMAD_VERSION}_linux_amd64.zip -o nomad.zip
echo "Installing Nomad version ${NOMAD_VERSION} ..."
unzip nomad.zip
sudo chmod +x nomad
sudo mv nomad /usr/bin/nomad
sudo mkdir /etc/nomad.d
sudo chmod a+w /etc/nomad.d
SCRIPT

# Specify a Consul version
CONSUL_VERSION = ENV['CONSUL_VERSION']

Vagrant.configure("2") do |config|

  config.vm.define "rvn11" do |rvn11|
    rvn11.vm.box = "ubuntu/lunar64"

    rvn11.vm.hostname = "rvn11.machine.local"
    rvn11.vm.network "public_network",  ip: "172.24.1.1/16", bridge: "tap0"
  end

  #config.vm.define "rvn12" do |rvn12|
  #  rvn12.vm.box = "ubuntu/lunar64"

  #  rvn12.vm.hostname = "rvn12.machine.local"
  #  rvn12.vm.network "public_network",  ip: "172.24.1.2", bridge: "tap0"
  #end

  config.vm.provision "shell", 
                      inline: $script,
                      env: {'CONSUL_VERSION' => CONSUL_VERSION}

  config.vm.box_check_update = false
end
