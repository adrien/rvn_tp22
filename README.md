# RVN - TP2.2 : Ordonnancement de services sur une grappe

1. Provisionner des VMs **sur notre WAN** avec [Vagrant](https://developer.hashicorp.com/vagrant/docs)
2. Déployer [Consul](https://developer.hashicorp.com/consul/docs) et [Nomad](https://developer.hashicorp.com/nomad/docs) 
3. Déployer des services 

## 1. Provisionner des VMs

1. Vérifiez le bon fonctionnement de notre WAN
2. Écrivez chacun un [Vagrantfile](./Vagrantfile) pour lancer une VM sur le WAN, en suivant le schéma d'adressage suivant :

| Continent| IP sur continent   | IP VM            |
|--------- | ------------------ | ---------------- |
| Amerique | `10.13.1.3/24`     | `10.13.1.31/24` |
| Europe   | `10.13.2.1/24`     | `10.13.2.11/24` |
| Asie     | `10.13.3.4/24`     | `10.13.3.41/24` |

3. Vérifiez que les machines puissent discuter entre elles.


Dans le Vagrantfile : 

```
config.vm.network "public_network",  ip: "10.13.4.31", bridge: "eno1"
```

Essayez d'accéder aux ports `8500` et `4646` de la VM depuis un navigateur.

## 2. Déployer Consul et Nomad

1. Modifiez votre `Vagrantfile` pour qu'il provisionne votre VM avec [Consul](https://developer.hashicorp.com/consul/docs) et [Nomad](https://developer.hashicorp.com/nomad/docs)

    Vous pouvez aussi lancer des scripts sur les VMs sans rebooter, par exemple avec :
       
        ssh ... ENV=xxx 'bash -s' < script.sh 

Inspiration : [./install_nomad_consul.sh](./install_nomad_consul.sh)

Chaque continent est un "datacenter" : `am`, `eu`, `as`. Il nous faut un Consul/Nomad serveur par zone géographique, les autres sont clients (configuration manuelle dans les `.hcl`).

## Docs 

* Configs Nomad Deuxfleurs (dans sous-dossiers `deploy/`) : https://git.deuxfleurs.fr/Deuxfleurs/nixcfg/src/branch/main/cluster/prod/app

### Consul

* Follow this guide to provision a Vagrant VM with Consul: https://developer.hashicorp.com/consul/tutorials/certification-associate-tutorials/get-started-create-datacenter

* Vagrant can auto-install Docker if you ask it to run containers: https://developer.hashicorp.com/vagrant/docs/provisioning/docker

* Deployment guide: https://developer.hashicorp.com/consul/tutorials/production-deploy/deployment-guide

* Register services to Consul: https://developer.hashicorp.com/consul/tutorials/get-started-vms/virtual-machine-gs-service-discovery

### Nomad

* Deployment guide: https://developer.hashicorp.com/nomad/tutorials/enterprise/production-deployment-guide-vm-with-consul

